const problem = require("../problem.cjs")
const data=require("../data.json")


problem.retrieve(2).then((data) => {
    return problem.groupDataBasedOnCompany(data)
}).then((data) => {
    return problem.getAllDataofPowerPuf(data)
}).then((data) => {
    return problem.removeId(data)
}).then((data) => {
    return problem.sortBasedOnComapnyNam(data)
}).then((data) => {
    return problem.swapCompanye(data)
}).then((data) => {
    return problem.addBirthdayIfIdIsEvene(data)
}).catch((error)=>{
    return error
})